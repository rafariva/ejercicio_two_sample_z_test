source('load_data.R')


#Se encuentran resultados anonimizados de 1543 grabaciones en el sistema RAP durante el primer semestre del 2018.
grupo_retro <- group_by(estudiantes_2_intentos_grupos, Retroalimentacion)
estadisticos <- summarize(grupo_retro, Promedio_Global = mean(Calificacion_global), SD_Global = sd(Calificacion_global))
View(estadisticos)

# Incluir aquí, analisis two-sample z-test

#¿Tiene algún efecto la retroalimentación del profesor versus la retroalimentación del sistema?
#Ho: es indiferente quién de la retroalimentación
#H1: el profesor da una mejor retroalimentación para tener una mejor calificacion.


N<-1543 #tamaño de poblacion
n<-count(estudiantes_2_intentos_grupos) #cantidad de muestra

prom_a <- estadisticos[1,2]  #promedio con el profesor
prom_b <- estadisticos[2,2]  #promedio con el sistema
SD_a <- estadisticos[1,3]    #desviacion con el profesor
SD_b <- estadisticos[2,3]    #desviacion con el sistema

SE_a <- SD_a/sqrt(n)  #error con el profesor
SE_b <- SD_b/sqrt(n)  #error con el sistema

SE_diferencia <- sqrt(SE_a^2+SE_b^2)  #diferencia del error

z <- ((prom_a-prom_b)-0)/SE_diferencia  #z-test
z<-z[1,1]
z
p_Value = pnorm(-abs(z)) ## p = 1.2??
p_Value


###############################################################################################
# p_value es 0.1876555,, mayor al 0.05 por lo que NO se rechaza la hipotesis nula, es decir, que da lo mismo
# si la retroalimentacion es del profesor o del sistema
###############################################################################################



